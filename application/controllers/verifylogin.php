<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   
 }

 function index()
 {
  
    $username=$this->input->post('name');
    $password=$this->input->post('password');

    if($this->user->login($username,$password)){
      echo "<script>alert('Welcome back');</script>";

      $this->load->library("JWT");

      $CONSUMER_KEY='Go1FTO9D2D16066i16g6ex6v56Wk08Yg';
      $CONSUMER_SECRET='Go1FTO9D2D16066i16g6ex6v56Wk08Yg';
      $CONSUMER_TTL=86400;

      $token=$this->jwt->encode(array(
        'consumerkey'=>$CONSUMER_KEY,
        'userId'=>$username,
        'issuedAt'=>date(DATE_ISO8601, strtotime("now")),
        'ttl'=>$CONSUMER_TTL

        ),$CONSUMER_SECRET);
      echo $token;



    }
    else{
      echo "<script>alert('Invalid email address or password');</script>";
      $this->load->view('home_view');
    }

 }


}