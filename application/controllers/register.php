<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){

		parent::__construct();
		$this->load->model('user','',TRUE);
	}
	public function index()
	{
		//$this->load->helper(array('form'));
		//$this->load->view('home_view');

		$username=$this->input->post('name');
		$password=$this->input->post('password');

		$result=$this->user->checkExistUser($username);

		if(!$result){

			$data=array(
				'username'=>$username,
				'password'=>md5($password)
				);

		
		$this->user->register($data);
		echo "<script>alert('You have success registered');</script>";
		$this->load->view('home_view');
	    }
		else{
			echo "<script>alert('Sorry, this email address has been used!');</script>";
			$this->load->view('register');
		}
}

}